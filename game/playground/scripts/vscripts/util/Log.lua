-- Contains helpful log methods
--
-- Usage:
--      Can directly call out to log a message at 5 levels [v,d,i,w,e]
--          Log:v("some msg")
--          Log:i("some msg")
--
--      Console:
--          V: some msg
--          I: some msg
--
--      The log level will be displayed in the dota Console, which can filter messages based on level
--      The levels in order of most important to least:
--          e=ERROR
--          w=WARNING
--          i=INFO
--          d=DEBUG
--          v=VERBOSE
--
--      Messages can also include a tag:
--          Log:w("tag", "some msg")
--
--      Console:
--          W: tag | some msg
--
--      Tags will be displayed in the console and developers can add additional filters based on them
--

LogLevel = {
    ERROR = 1,
    WARNING = 2,
    INFO = 3,
    DEBUG = 4,
    VERBOSE = 5
}

if not Log then
    Log = {}
    Log.__index = Log

    -- Init
    Log.logLevel = LogLevel.VERBOSE
    Log.logLevelPrefix = {}
    Log.logLevelPrefix[LogLevel.ERROR] = "E: "
    Log.logLevelPrefix[LogLevel.WARNING] = "W: "
    Log.logLevelPrefix[LogLevel.INFO] = "I: "
    Log.logLevelPrefix[LogLevel.DEBUG] = "D: "
    Log.logLevelPrefix[LogLevel.VERBOSE] = "V: "
end

function Log:v(msg)
    self:log(LogLevel.VERBOSE, msg)
end

function Log:d(msg)
    self:log(LogLevel.DEBUG, msg)
end

function Log:i(msg)
    self:log(LogLevel.INFO, msg)
end

function Log:w(msg)
    self:log(LogLevel.WARNING, msg)
end

function Log:e(msg)
    self:log(LogLevel.ERROR, msg)
end

function Log:v(tag, msg)
    self:log(LogLevel.VERBOSE, tag, msg)
end

function Log:d(tag, msg)
    self:log(LogLevel.DEBUG, tag, msg)
end

function Log:i(tag, msg)
    self:log(LogLevel.INFO, tag, msg)
end

function Log:w(tag, msg)
    self:log(LogLevel.WARNING, tag, msg)
end

function Log:e(tag, msg)
    self:log(LogLevel.ERROR, tag, msg)
end

function Log:log(level, tag, msg)
    if not level or not tag then
        print("Invalid parameters passed to log: level=" .. tostring(level) .. " | tag=" .. tostring(tag) .. " | msg=" .. tostring(msg))
        return
    end
    if not msg then
        -- Lua can't send parameters in between. If no msg sent, assume tag was the message
        msg = tag
        tag = nil
    end

    -- Filter out messages at too low of a log level
    if level > self.logLevel then
        return
    end

    -- Filters out messages based on tags
    if self.tagWhitelist and not self.tagWhitelist[tag] then
        -- This tag is being filtered out
        return
    end
    if self.tagBlacklist and self.tagBlacklist[tag] then
        -- This tag is being filtered out
        return
    end

    local prefix = self.logLevelPrefix[level] or ""
    if not tag then
        tag = ""
    else
        tag = tostring(tag) .. " | "
    end

    if type(msg) == "table" then
        print(prefix .. tag)
        PrintTable(msg)
    else
        print(prefix .. tag .. msg)
    end
end

function Log:setLogLevel(level)
    if not level or type(level) ~= "number" or level < 1 then
        self:e("Can't set log level to: " .. tostring(level))
        return
    end
    print("Log level updated to: " .. tostring(level))
    self.logLevel = level
end

-- Adds the provided tag to the whitelist. This means all
-- tags not in the whitelist are filtered out
function Log:whitelistTag(tag)
    if not tag or type(tag) ~= "string" then
        self:e("Can't whitelist tag: " .. tostring(level))
        return
    end
    if not self.tagWhitelist then
        self.tagWhitelist = {}
    end
    print("Adding '" .. tostring(tag) .. "' to whitelist")
    self.tagWhitelist[tag] = true
end

-- Adds the provided tag to the blacklist. This means all
-- tags on the blacklist are filtered out
function Log:blacklistTag(tag)
    if not tag or type(tag) ~= "string" then
        self:e("Can't blacklist tag: " .. tostring(level))
        return
    end
    if not self.tagBlacklist then
        self.tagBlacklist = {}
    end
    print("Adding '" .. tostring(tag) .. "' to blacklist")
    self.tagBlacklist[tag] = true
end

-- Clears the whitelist, removing filters based on tags
function Log:clearTagFilters()
    print("Clearing tag filters")
    if self.tagWhitelist then
        self.tagWhitelist = nil
    end
    if self.tagBlacklist then
        self.tagBlacklist = nil
    end
end

-- Adds log functions to the provided class so all the class
-- has to do is call self:[logv, logd, logi, logw, loge] to emit the log
-- with the tag provided in this method
-- @param tag | The tag to attach to all logs emitted from one of the emitted functions
function Log:addLogFunctions(class, tag)
    if not tag then
        Log:e("addLogFunction requires a tag!")
        tag.thisMustNotBeNull = nil -- Throw exception to get the trace
    end
    class.logv = function(self, msg)
        Log:v(tag, msg)
    end
    class.logd = function(self, msg)
        Log:d(tag, msg)
    end
    class.logi = function(self, msg)
        Log:i(tag, msg)
    end
    class.logw = function(self, msg)
        Log:w(tag, msg)
    end
    class.loge = function(self, msg)
        Log:e(tag, msg)
    end
end
