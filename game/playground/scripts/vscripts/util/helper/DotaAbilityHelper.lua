-- Util functions involving Dota Abilities

-- Returns true/false if the ability has the specified behavior
-- (This was made into a util function because dota made the GetBehavior() function
-- return a userdata instead of an int and the workaround is a bit hacky)
-- Ex: AbilityHasBehavior(ability, DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE)
function AbilityHasBehavior(ability, behaviorCode)
    local abilityBehavior = ability:GetBehavior()
    abilityBehavior = tonumber(tostring(abilityBehavior)) -- Hacky way to convert to int, but seems to work
    return hasbit(abilityBehavior, behaviorCode)
end

-- Swaps two abilities on a unit. The unit must already have both passed in abilities
-- @param onAbility | The ability to be swapped in and shown
-- @param offAbility | The ability to be swapped out and hidden
function SwapAbilities(unit, onAbility, offAbility)
    unit:SwapAbilities(onAbility, offAbility, true, false)
end

-- See [SwapAbilities] but callable from datadriven txt files using:
--[[
    "RunScript"
    {
        "ScriptFile"	"util/DotaAbilityHelper.lua"
        "Function"		"SwapAbilitiesDataDriven"
        "on_ability"    "forcefield_off"
        "off_ability"    "forcefield_on"
    }
--]]
function SwapAbilitiesDataDriven(keys)
    SwapAbilitiesLua(keys.caster, keys.on_ability, keys.off_ability)
end

--- Abort the cast of an ability and refunds mana spent to cast it
-- @param ability The ability to abort/refresh
-- @param errorMessage The error message, hopefully a #key in the panorama file
-- @param errorMessageArgs The argument parameters to fill into the errorMessage
function AbortAbilityCast(ability, errorMessage, errorMessageArgs)
    local caster = ability:GetCaster()
    local pID = caster:GetPlayerOwnerID()

    caster:Interrupt()
    SendErrorMessage(pID, errorMessage, errorMessageArgs)

    -- refund and reset the cooldown
    local mana = caster:GetMana()
    Timers:CreateTimer(function()
        ability:EndCooldown()
        caster:SetMana(mana)
    end)
end
