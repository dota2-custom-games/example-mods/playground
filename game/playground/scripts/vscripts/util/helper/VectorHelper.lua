-- useful functions on vectors

function distanceBetweenVectors(vectorA, vectorB)
    return math.sqrt(math.pow(vectorA.x - vectorB.x, 2) + math.pow(vectorA.y - vectorB.y, 2))
end

-- Returns a vector with values of 0,maxSize in both the x and y directions
-- This differs from RandomVector(size) which returns a Vector in a random direction and random amplitude of the supplied size
-- Ex: RandomSizedVector(10) -> Vector(0, 10, 0), Vector (0, 1, 0), Vector(-3, -2, 0)...etc
function RandomSizedVector(maxSize)
    return Vector(RandomInt(-1 * maxSize, maxSize), RandomInt(-1 * maxSize, maxSize), 0)
end

-- Returns angle between two vectors in degrees
-- Note: Only cares about (x,y) coordinates
function AngleBetweenVectors2D(a, b)
    return (math.atan2(a.y, a.x) - math.atan2(b.y, b.x)) * 180 / math.pi
end
