-- Useful functions to apply to numbers

-- Rounds the passed positive double to the nearest integer
function Round(x)
    return math.floor(x + 0.5)
end

-- rounds a number to the nearest decimal places
-- Ex: (1.23456, 3) -> 1.235
function Round(val, decimal)
  if (decimal) then
    return math.floor( (val * 10^decimal) + 0.5) / (10^decimal)
  else
    return math.floor(val+0.5)
  end
end

-- Returns true if the binary representation of x has the bit at position p set to 1
function hasbit(x, p)
    return x % (p + p) >= p
end

-- add comma to separate thousands
-- Ex: 1000000 -> "1,000,000"
function CommaNumber(amount)
  local formatted = amount
  for i=1,100 do
    formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
    if (k==0) then
      break
    end
  end
  return formatted
end

-- Returns the angle (in degrees) within the range of 0..<360 degrees
function CorrectAngle(angle)
    while (angle >= 360) or (angle < 0) do
        if angle >= 360 then
            angle = angle - 360
        else
            angle = angle + 360
        end
    end
    return angle
end
