-- Useful functions for string manipulation

-- Splits the string based on the separator passed in
function string:split(sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(self, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

-- Returns true if the provided string begins with the substring passed in
function string:startsWith(substring)
    local strlen = self.len(substring)
    local first_characters = string.sub(self, 1 , strlen)
    return (first_characters == substring)
end
