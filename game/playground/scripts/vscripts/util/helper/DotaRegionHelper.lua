-- Utility methods for Dota Regions

LOCATIONS_RANDOM_POINT_OFFSET = 120 -- to try and prevent things spawning inside/on walls or something
DUMMY_LOCATION = Vector(0,0,0) -- Vector to spawn dummy units

-- Returns the center of a region
function GetCenterInRegion(region)
    return region:GetCenter()
end

-- Returns a random vector within the provided region
-- @param regionGroup | Table whose values are Dota regions
-- @return vector | Random vector in the region group
function GetRandomPointInRegion(region)
    local bounds = region:GetBounds()
    return region:GetOrigin() + Vector(RandomFloat(bounds.Mins.x + LOCATIONS_RANDOM_POINT_OFFSET, bounds.Maxs.x - LOCATIONS_RANDOM_POINT_OFFSET), RandomFloat(bounds.Mins.y + LOCATIONS_RANDOM_POINT_OFFSET, bounds.Maxs.y - LOCATIONS_RANDOM_POINT_OFFSET), 0)
end

-- Returns a random vector within the provided region group
-- @param regionGroup | Table whose values are Dota regions
-- @return vector | Random vector in the region group
function GetRandomPointInRegionGroup(regionGroup)
    local region = regionGroup[RandomInt(1, #regionGroup)]
    local bounds = region:GetBounds()
    return region:GetOrigin() + Vector(RandomFloat(bounds.Mins.x + LOCATIONS_RANDOM_POINT_OFFSET, bounds.Maxs.x - LOCATIONS_RANDOM_POINT_OFFSET), RandomFloat(bounds.Mins.y + LOCATIONS_RANDOM_POINT_OFFSET, bounds.Maxs.y - LOCATIONS_RANDOM_POINT_OFFSET), 0)
end

-- Returns true if the supplied vector position is in the region
-- @param pos | Vector to check if is inside the region
-- @param region | Dota region to check for
function VectorIsInRegion(pos, region)
    local regionOrigin = region:GetOrigin()
    local bounds = region:GetBounds()

    return pos.x >= (regionOrigin.x + bounds.Mins.x)
            and pos.x <= (regionOrigin.x + bounds.Maxs.x)
            and pos.y >= (regionOrigin.y + bounds.Mins.y)
            and pos.y <= (regionOrigin.y + bounds.Maxs.y)
end

-- Returns true if the unit's abs origin is in the region.
-- This does NOT include any portion of the units' bounds.
-- See: UnitBoundsAreInRegion for that.
-- @param unit | Dota entity to check for
-- @param region | Dota region to check for
function UnitIsInRegion(unit, region)
    local regionOrigin = region:GetOrigin()
    local bounds = region:GetBounds()
    local unitPos = unit:GetAbsOrigin()

    return unitPos.x >= (regionOrigin.x + bounds.Mins.x)
            and unitPos.x <= (regionOrigin.x + bounds.Maxs.x)
            and unitPos.y >= (regionOrigin.y + bounds.Mins.y)
            and unitPos.y <= (regionOrigin.y + bounds.Maxs.y)
end

-- Returns true if the unit is within any region in the region group
-- @param unit | Dota entity to check for
-- @param regionGroup | Table whose values are Dota regions
function UnitIsInRegionGroup(unit, regiongroup)
    for _,region in pairs(regiongroup:GetChildren()) do
        if UnitIsInRegion(unit, region) then
            return true
        end
    end
    return false
end

-- Returns true if the bounds of the unit intersect with the bounds
-- of the region.
-- @param unit | Dota entity to check for
-- @param region | Dota region to check for
function UnitBoundsAreInRegion(unit, region)
    local regionOrigin = region:GetOrigin()
    local regionBounds = region:GetBounds()
    local unitOrigin = unit:GetAbsOrigin()
    local unitBounds = unit:GetBounds()

    return (unitOrigin.x + unitBounds.Mins.x) < (regionOrigin.x + regionBounds.Maxs.x)
        and (unitOrigin.x + unitBounds.Maxs.x) > (regionOrigin.x + regionBounds.Mins.x)
        and (unitOrigin.y + unitBounds.Mins.y) < (regionOrigin.y + regionBounds.Maxs.y)
        and (unitOrigin.y + unitBounds.Maxs.y) > (regionOrigin.y + regionBounds.Mins.y)
end
