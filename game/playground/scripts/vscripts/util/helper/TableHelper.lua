-- useful functions on tables

-- Prints out a table  recursively, with some formatting helpers for dota userdata's
function PrintTable(t, indent, done)
  --print ( string.format ('PrintTable type %s', type(keys)) )
  if type(t) ~= "table" then return end

  done = done or {}
  done[t] = true
  indent = indent or 0

  local l = {}
  for k, v in pairs(t) do
    table.insert(l, k)
  end

  table.sort(l)
  for k, v in ipairs(l) do
    -- Ignore FDesc
    if v ~= 'FDesc' then
      local value = t[v]

      if type(value) == "table" and not done[value] then
        done [value] = true
        print(string.rep ("\t", indent)..tostring(v)..":")
        PrintTable (value, indent + 2, done)
      elseif type(value) == "userdata" and not done[value] then
        done [value] = true
        print(string.rep ("\t", indent)..tostring(v)..": "..tostring(value))
        PrintTable ((getmetatable(value) and getmetatable(value).__index) or getmetatable(value), indent + 2, done)
      else
        if t.FDesc and t.FDesc[v] then
          print(string.rep ("\t", indent)..tostring(t.FDesc[v]))
        else
          print(string.rep ("\t", indent)..tostring(v)..": "..tostring(value))
        end
      end
    end
  end
end

function table.size(table)
    local size = 0
    for _,_ in pairs(table) do
        size = size + 1
    end
    return size
end

-- Returns true if the table contains the value at least one time
function table.contains(table, value)
    for k,v in pairs(table) do
        if v == value then
            return true
        end
    end
    return false
end

-- Returns the first key that has the passed in value or nil if none have it
function table.keyForValue(table, value)
    for k,v in pairs(table) do
        if v == value then
            return k
        end
    end
    return nil
end

-- Returns a string of the list in the form:
-- { key=value, key2=value2 }
function table.toPrettyString(table, value)
    for k,v in pairs(table) do
        if v == value then
            return k
        end
    end
    return nil
end

function table.isEmpty(myTable)
    if next(myTable) == nil then
       return true
    else
        return false
    end
end

function table.isNotEmpty(myTable)
    if next(myTable) == nil then
       return false
    else
        return true
    end
end
