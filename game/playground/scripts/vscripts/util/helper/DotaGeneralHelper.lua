-- General DOTA helper functions

-- Used for stat tracking and display, this is the "official" time for how long the game was in seconds
-- It should match the in game clock
function GetGameDuration()
    -- False/False seems to be what matches the in game clock the best
    return GameRules:GetDOTATime(false, false)
end

-- Returns a random traversable position within the specified range of the position
function RandomTraversablePosition(position, min, max)
    local randomPosition = nil
    for i=1,10 do
        local pos = position + RandomVector(RandomFloat(min,max))
        if GridNav:IsTraversable(pos) then
            randomPosition = pos
            break
        end
    end
    if not randomPosition then
        -- Didn't find a valid random position, just use the position then
        randomPosition = position
    end
    return randomPosition
end

-- Converts the initial player team to the player index
-- Ex: DOTA_GOOD_GUYS == 1, DOTA_CUSTOM_1 == 2, DOTA_CUSTOM_8 == 9
function TeamToPlayerIndex(playerTeam)
    local playerIndex = nil
    if playerTeam == DOTA_TEAM_GOODGUYS then
        playerIndex = 1
    else
        -- Custom teams all increment by one and we want custom team 1 to be index 2
        playerIndex = playerTeam - DOTA_TEAM_CUSTOM_1 + 2
    end
    return playerIndex
end
