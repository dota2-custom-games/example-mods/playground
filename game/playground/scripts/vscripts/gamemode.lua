PLAYGROUND_VERSION = "0.0.1"

-- Creating a global gamemode variable;
if GameMode == nil then
    -- _G.GameMode = class({})
    GameMode = {}
    GameMode.__index = GameMode
end

-- Load utils
require('util/Log')
require('util/Check')
require('util/helper/DotaAbilityHelper')
require('util/helper/DotaGeneralHelper')
require('util/helper/DotaItemHelper')
require('util/helper/DotaRegionHelper')
require('util/helper/DotaUnitHelper')
require('util/helper/ListHelper')
require('util/helper/NumberHelper')
require('util/helper/PrintHelper')
require('util/helper/SetHelper')
require('util/helper/StringHelper')
require('util/helper/TableHelper')
require('util/helper/VectorHelper')
require('util/RegionListeners')
require('util/DebugCommands')
require('util/Popups')

require('libraries/timers') -- Many libraries are dependent on timers, require it here
-- Projectiles library can be used for advanced 3D projectile systems.
require('libraries/projectiles')
-- Notifications library can be used for sending panorama notifications to the UIs of players/teams/everyone
require('libraries/notifications')
-- Animations library can be used for starting customized animations on units from lua
require('libraries/animations')
-- Attachments library can be used for performing "Frankenstein" attachments on units
require('libraries/attachments')
-- Selection library (by Noya) provides player selection inspection and management from server lua
require('libraries/selection')
-- Illusion library (by Darkonius)
require('libraries/custom_illusions')
-- Containers Library (by BMD, modified by Shooper) allows for custom shops and inventories
require('libraries/containers')
-- Player Resource Library (by Darkonius) extends standard playerresource class to add utility functions
require('libraries/player_resource')
-- Building Helper Library (By Noya) adds tools for building-creating units
require('libraries/buildinghelper')

-- Interfaces for handling specific families of particles
require('particle_handlers/attack_telegraphs')
require('particle_handlers/objective_markers')

-- Interfaces for handling panorama additions
require('panorama_interfaces/MinimapManager')
require('panorama_interfaces/ProgressBars')

-- filters.lua contains triggers for things like damage, healing, etc.
require('filters/filters')


require('players/CustomStats')

-- Load a Game Manager, which handles normal execution
require('game/GameManager')

-- PlaygroundManager for individual testing of things
require('game/PlaygroundManager')

-- Extension class for players that 
require('players/PlayerManager')

--[[
  This function should be used to set up Async precache calls at the beginning of the gameplay.

  In this function, place all of your PrecacheItemByNameAsync and PrecacheUnitByNameAsync.  These calls will be made
  after all players have loaded in, but before they have selected their heroes. PrecacheItemByNameAsync can also
  be used to precache dynamically-added datadriven abilities instead of items.  PrecacheUnitByNameAsync will
  precache the precache{} block statement of the unit and all precache{} block statements for every Ability#
  defined on the unit.

  This function should only be called once.  If you want to/need to precache more items/abilities/units at a later
  time, you can call the functions individually (for example if you want to precache units in a new wave of
  holdout).

  This function should generally only be used if the Precache() function in addon_game_mode.lua is not working.
]]
function GameMode:PostLoadPrecache()
	--PrecacheItemByNameAsync("item_example_item", function(...) end)
	--PrecacheItemByNameAsync("example_ability", function(...) end)

	--PrecacheUnitByNameAsync("npc_dota_hero_viper", function(...) end)
	--PrecacheUnitByNameAsync("npc_dota_hero_enigma", function(...) end)
end

--[[
  This function is called once and only once for every player when they spawn into the game for the first time.  It is also called
  if the player's hero is replaced with a new hero for any reason.  This function is useful for initializing heroes, such as adding
  levels, changing the starting gold, removing/adding abilities, adding physics, etc.

  The hero parameter is the hero entity that just spawned in
]]
function GameMode:OnHeroInGame(hero)

	--------- Playground Stuff ------------

    local alreadyLoaded = self.playersLoadingCount
    if not alreadyLoaded then
        self.playersLoadingCount = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_GOODGUYS)
        for i = DOTA_TEAM_CUSTOM_1, DOTA_TEAM_CUSTOM_8 do
            self.playersLoadingCount = self.playersLoadingCount + PlayerResource:GetPlayerCountForTeam(i)
        end

        -- CODE RUN ON FIRST HERO LOAD

        -- Update the team max size
        GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_GOODGUYS, 9)

        -- Set the "good guys" team to have the correct colour
        -- We changed it in the loading screen for chat colour reasons
        SetTeamCustomHealthbarColor(DOTA_TEAM_GOODGUYS,  140, 203, 98)

        self:logv("onPreGameStarted")

        PlayerManager:onPreGameStarted()
        GameManager:onPreGameStarted()

    end

    local playerId = hero:GetPlayerOwnerID()
    local player = PlayerResource:GetPlayer(playerId)
    CustomGameEventManager:Send_ServerToPlayer(player, "player_loaded", {playerId=playerId}) -- TODO: Still necessary?

    -- Let player manager set up the initial dummy model
    PlayerManager:onPlayerHeroInGame(hero)
    GameManager:onPlayerHeroInGame(hero)

    self.playersLoadingCount = self.playersLoadingCount - 1
    if self.playersLoadingCount == 0 then
        self:logv("All players loaded")
        PlayerManager:onAllPlayersLoadedGame()
        GameManager:onAllPlayersLoadedGame()
    end

end

-- MARK: Game Initialization

-- This function initializes the game mode and is called before anyone loads into the game
function GameMode:InitGameMode()
    Log:addLogFunctions(GameMode, "GameMode")

    -- Set up initial DOTA rules
    GameMode:SetUpRules()

    -- Set up DOTA game event listeners
    GameMode:RegisterDotaGameEventListeners()

	-- Change random seed for math.random function
	local timeTxt = string.gsub(string.gsub(GetSystemTime(), ':', ''), '0','')
	math.randomseed(tonumber(timeTxt))

	local gameModeEntity = GameRules:GetGameModeEntity()

	-- Setting the Order filter
	gameModeEntity:SetExecuteOrderFilter(Dynamic_Wrap(GameMode, "OrderFilter"), self)

	-- Setting the Damage filter
	gameModeEntity:SetDamageFilter(Dynamic_Wrap(GameMode, "DamageFilter"), self)

	-- Setting the Modifier filter
	gameModeEntity:SetModifierGainedFilter(Dynamic_Wrap(GameMode, "ModifierFilter"), self)

	-- Setting the Experience filter
	gameModeEntity:SetModifyExperienceFilter(Dynamic_Wrap(GameMode, "ExperienceFilter"), self)

	-- Setting the Tracking Projectile filter
	gameModeEntity:SetTrackingProjectileFilter(Dynamic_Wrap(GameMode, "ProjectileFilter"), self)

	-- Setting the rune spawn filter
	gameModeEntity:SetRuneSpawnFilter(Dynamic_Wrap(GameMode, "RuneSpawnFilter"), self)

	-- Setting the bounty rune pickup filter
	gameModeEntity:SetBountyRunePickupFilter(Dynamic_Wrap(GameMode, "BountyRuneFilter"), self)

	-- Setting the Healing filter
	gameModeEntity:SetHealingFilter(Dynamic_Wrap(GameMode, "HealingFilter"), self)

	-- Setting the Gold Filter
	gameModeEntity:SetModifyGoldFilter(Dynamic_Wrap(GameMode, "GoldFilter"), self)

	-- Setting the Inventory filter (seems to be buggy?)
	--GameMode:SetItemAddedToInventoryFilter(Dynamic_Wrap(GameMode, "InventoryFilter"), self)

	-- Global Lua Modifiers
	LinkLuaModifier("modifier_custom_invulnerable", "modifiers/modifier_custom_invulnerable", LUA_MODIFIER_MOTION_NONE)

    self:logv("GameMode initialized")

    GameMode = self

    -- Load skills_kv tables what used for "normal" usage of level 20+ skills and make skill swapping easier
    SKILL_KV = LoadKeyValues("scripts/kv/skills_kv.kv") or {}

    -- Index the npc/items - allows lookup of values in the individual item's file
    GameMode.item_infos = LoadKeyValues("scripts/npc/npc_items_custom.txt")

    -- Index the npc units and npc heroes values
    -- Note: To retrieve a value:
    --        local unit_value = GameMode.unit_infos[unit:GetUnitName()]
    --        unit_value["MyCustomKey"]
    GameMode.unit_infos = LoadKeyValues("scripts/npc/npc_units_custom.txt")
    for k, v in pairs(LoadKeyValues("scripts/npc/npc_heroes_custom.txt")) do
        GameMode.unit_infos[k] = v
        -- Note: Some Heroes GetUnitName() returns the overridden_hero
        -- So, make it such that any overridden hero is also in the map and points to the txt file that overrides them
        -- Ex: npc_dota_hero_sniper -> npc_playground's values
        local override = GameMode.unit_infos[k]["override_hero"]
        if override then
            GameMode.unit_infos[override] = v
        end
    end

    -- Removes item buying limit
    cvar_setf("dota_max_physical_items_purchase_limit", 1000)
end

-- MARK: Dota Custom Rules

-- Sets up game rules, overriding many standard DOTA parameters
function GameMode:SetUpRules()
    GameRules:SetHeroRespawnEnabled(false)       -- Should the heroes automatically respawn on a timer or stay dead until manually respawned
    GameRules:SetUseUniversalShopMode(false)     -- Should the shops contain all items?
    GameRules:SetSameHeroSelectionEnabled(false) -- Should we let people select the same hero as each other

    GameRules:SetHeroSelectionTime(0.0) -- How long should we let people select their hero? Should be at least 5 seconds.
    GameRules:SetPreGameTime(0.0)       -- How long after showcase time should the horn blow and the game start?
    GameRules:SetPostGameTime(60.0)     -- How long should we let people stay around before closing the server automatically?
    GameRules:SetShowcaseTime(0.0)      -- How long should show case time be?
    GameRules:SetStrategyTime(0.0)      -- How long should strategy time last? Bug: You can buy items during strategy time and it will not be spent!

    GameRules:SetTreeRegrowTime(300.0) -- How long should it take individual trees to respawn after being cut down/destroyed?

    GameRules:SetGoldPerTick(0)  -- How much gold should players get per tick?
    GameRules:SetGoldTickTime(0) -- How long should we wait in seconds between gold ticks?

    GameRules:SetHeroMinimapIconScale(1)               -- What icon size should we use for our heroes?
    GameRules:SetCreepMinimapIconScale(1)              -- What icon size should we use for creeps?
    GameRules:SetRuneMinimapIconScale(1)               -- What icon size should we use for runes?
    GameRules:SetFirstBloodActive(false)               -- Should we enable first blood for the first kill in this game?
    GameRules:SetHideKillMessageHeaders(true)          -- Should we hide the kill banners that show when a player is killed?
    GameRules:LockCustomGameSetupTeamAssignment(false) -- Should we Lock (true) or unlock (false) team assignemnt. If team assignment is locked players cannot change teams.

    GameRules:SetUseBaseGoldBountyOnHeroes(false)

    -- Set Team Sizes
    GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_GOODGUYS, 1)
    GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_BADGUYS, 1)
    GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_1, 1)
    GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_2, 1)
    GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_3, 1)
    GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_4, 1)
    GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_5, 1)
    GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_6, 1)
    GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_7, 1)
    GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_8, 1)

    -- Set Team Colors
    local teamColors = {}
    teamColors[DOTA_TEAM_GOODGUYS] = { 61, 210, 150 }  --    Teal
    teamColors[DOTA_TEAM_BADGUYS]  = { 243, 201, 9 }   --    Yellow
    teamColors[DOTA_TEAM_CUSTOM_1] = { 197, 77, 168 }  --    Pink
    teamColors[DOTA_TEAM_CUSTOM_2] = { 255, 108, 0 }   --    Orange
    teamColors[DOTA_TEAM_CUSTOM_3] = { 52, 85, 255 }   --    Blue
    teamColors[DOTA_TEAM_CUSTOM_4] = { 101, 212, 19 }  --    Green
    teamColors[DOTA_TEAM_CUSTOM_5] = { 129, 83, 54 }   --    Brown
    teamColors[DOTA_TEAM_CUSTOM_6] = { 27, 192, 216 }  --    Cyan
    teamColors[DOTA_TEAM_CUSTOM_7] = { 199, 228, 13 }  --    Olive
    teamColors[DOTA_TEAM_CUSTOM_8] = { 140, 42, 244 }   --    Purple
    for team,color in pairs(teamColors) do
        SetTeamCustomHealthbarColor(team, color[1], color[2], color[3])
    end

    -- XP values are set later
    -- GameRules:SetUseCustomHeroXPValues(true)

    -- More rules will be set up once the first player has loaded
    -- See: GameMode:AdditionalRuleSetup()
    ListenToGameEvent('player_connect_full', Dynamic_Wrap(GameMode, 'AdditionalRuleSetup'), self)
end

-- This function is called as the first player loads and sets up additional game mode parameters
-- See: GameMode:SetUpRules
function GameMode:AdditionalRuleSetup()
	local mode = GameRules:GetGameModeEntity()

    -- Forces all players to load in with a specific hero
    mode:SetCustomGameForceHero("npc_dota_hero_clinkz")

	-- Set GameMode parameters
	mode:SetRecommendedItemsDisabled(false)       -- Should we disable the recommended builds for heroes
	mode:SetCameraDistanceOverride(1134.0)        -- How far out should we allow the camera to go? 1134 is the default in Dota
	mode:SetBuybackEnabled(true)                  -- Should we allow people to buyback when they die?
	mode:SetCustomBuybackCostEnabled(false)       -- Should we use a custom buyback cost setting?
	mode:SetCustomBuybackCooldownEnabled(false)   -- Should we use a custom buyback time?
	mode:SetTopBarTeamValuesOverride(true)        -- Should we do customized top bar values or use the default kill count per team?
	mode:SetTopBarTeamValuesVisible(false)        -- Should we display the top bar score/count at all?

    -- mode:SetUseCustomHeroLevels(false)                            -- Should we use custom XP values to level up heroes, or the default Dota numbers?
    -- mode:SetCustomXPRequiredToReachNextLevel(XP_PER_LEVEL_TABLE)  -- Use a table in the form { level -> XP_NEEDED }

	mode:SetBotThinkingEnabled(false)             -- Should we have bots act like they would in Dota? (This requires 3 lanes, normal items, etc)
	mode:SetTowerBackdoorProtectionEnabled(false) -- Should we enable backdoor protection for our towers?

	mode:SetFogOfWarDisabled(false)               -- Should we disable fog of war entirely for both teams?
    mode:SetUnseenFogOfWarEnabled(true)          -- Should we make unseen and fogged areas of the map completely black until uncovered by each team?

	mode:SetGoldSoundDisabled(false)              -- Should we disable the gold sound when players get gold?
	mode:SetRemoveIllusionsOnDeath(false)         -- Should we remove all illusions if the main hero dies? BUGGED!?

	mode:SetAlwaysShowPlayerInventory(false)      -- Should we only allow players to see their own inventory even when selecting other units?
	mode:SetAnnouncerDisabled(true)               -- Should we disable the announcer from working in the game?
	mode:SetFixedRespawnTime(-1)                  -- What time should we use for a fixed respawn timer?  Use -1 to keep the default dota behavior.
	mode:SetFountainConstantManaRegen(-1)         -- What should we use for the constant fountain mana regen?  Use -1 to keep the default dota behavior.
	mode:SetFountainPercentageHealthRegen(-1)     -- What should we use for the percentage fountain health regen?  Use -1 to keep the default dota behavior.
	mode:SetFountainPercentageManaRegen(-1)       -- What should we use for the percentage fountain mana regen?  Use -1 to keep the default dota behavior.
	mode:SetLoseGoldOnDeath(false)                -- Should we have players lose the normal amount of dota gold on death?
	mode:SetMaximumAttackSpeed(700)               -- What should we use for the maximum attack speed?
	mode:SetMinimumAttackSpeed(10)                -- What should we use for the minimum attack speed?
	mode:SetStashPurchasingDisabled(true)         -- Should we prevent players from being able to buy items into their stash when not at a shop?

    mode:SetBountyRuneSpawnInterval(-1)           -- How long in seconds should we wait between bounty rune spawns? BUGGED! WORKS FOR POWERUPS TOO!
    mode:SetPowerRuneSpawnInterval(-1)            -- How long in seconds should we wait between power-up runes spawns? BUGGED! WORKS FOR BOUNTIES TOO!

	mode:SetDaynightCycleDisabled(false)          -- Should we disable the day night cycle from naturally occurring? (Manual adjustment still possible)
	mode:SetKillingSpreeAnnouncerDisabled(true)   -- Shuold we disable the killing spree announcer?
	mode:SetStickyItemDisabled(false)             -- Should we disable the sticky item button in the quick buy area?
	mode:SetPauseEnabled(true)                    -- Should we allow players to pause the game?
	mode:SetCustomScanCooldown(10000000)          -- Custom cooldown of Scan in seconds. Doesn't affect Scan's starting cooldown.
end

-- MARK: DOTA Game Events

-- Called at once the game initializes. This registers listeners to specific dota game events
function GameMode:RegisterDotaGameEventListeners()
    ListenToGameEvent('game_rules_state_change', Dynamic_Wrap(GameMode, 'OnGameRulesStateChange'), self)
    ListenToGameEvent('player_disconnect', Dynamic_Wrap(GameMode, 'OnDisconnect'), self)
    ListenToGameEvent("player_reconnected", Dynamic_Wrap(GameMode, 'OnPlayerReconnect'), self)
    ListenToGameEvent('npc_spawned', Dynamic_Wrap(GameMode, 'OnNPCSpawned'), self)
    ListenToGameEvent('dota_player_pick_hero', Dynamic_Wrap(GameMode, 'OnPlayerPickHero'), self)
    ListenToGameEvent('entity_killed', Dynamic_Wrap(GameMode, 'OnEntityKilled'), self)
end

-- Dota Game Event Listener: "game_rules_state_change"
-- The overall game state has changed. Required to listen for specific game events like all players loaded
function GameMode:OnGameRulesStateChange(keys)
    local new_state = GameRules:State_Get()

    if new_state == DOTA_GAMERULES_STATE_INIT then
    elseif new_state == DOTA_GAMERULES_STATE_WAIT_FOR_PLAYERS_TO_LOAD then
    elseif new_state == DOTA_GAMERULES_STATE_CUSTOM_GAME_SETUP then
        -- How long should custom game setup last - the screen where players pick a team?
        GameRules:SetCustomGameSetupAutoLaunchDelay(25.0)
    elseif new_state == DOTA_GAMERULES_STATE_HERO_SELECTION then
        GameMode:PostLoadPrecache()

        -- Timers:CreateTimer(HERO_SELECTION_TIME+STRATEGY_TIME-1, function()
        --     for playerID = 0, 19 do
        --         if PlayerResource:IsValidPlayerID(playerID) then
        --             -- If this player still hasn't picked a hero, random one
        --             if not PlayerResource:HasSelectedHero(playerID) and PlayerResource:IsConnected(playerID) and (not PlayerResource:IsBroadcaster(playerID)) then
        --                 PlayerResource:GetPlayer(playerID):MakeRandomHeroSelection()
        --                 PlayerResource:SetHasRandomed(playerID)
        --                 PlayerResource:SetCanRepick(playerID, false)
        --             end
        --         end
        --     end
        -- end)
    elseif new_state == DOTA_GAMERULES_STATE_STRATEGY_TIME then
    elseif new_state == DOTA_GAMERULES_STATE_TEAM_SHOWCASE then
    elseif new_state == DOTA_GAMERULES_STATE_WAIT_FOR_MAP_TO_LOAD then
    elseif new_state == DOTA_GAMERULES_STATE_PRE_GAME then
    elseif new_state == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
        -- Game timer is at 0:00 instead of negative
    elseif new_state == DOTA_GAMERULES_STATE_POST_GAME then
    elseif new_state == DOTA_GAMERULES_STATE_DISCONNECT then
    end
end

-- Dota Game Event Listener: "player_disconnect"
-- Called when a player disconnects
function GameMode:OnDisconnect(keys)
    local name = keys.name
    local networkID = keys.networkid
    local reason = keys.reason
    local userID = keys.userid

    local playerId = keys.PlayerID
    local playerInfo = g_PlayerManager:getPlayerInfoForPlayerId(playerId)

    if playerInfo then
        PlayerManager:onPlayerDisconnect(playerInfo)
        -- GameManager will be alerted inside onPlayerReconnect of PlayerManager
    else
        self:logw("Player ID=" .. tostring(playerId) .. " disconnected but there is no playerInfo!")
    end
end

-- Dota Game Event Listener: "player_reconnected"
-- A player has reconnected to the game.
function GameMode:OnPlayerReconnect(keys)
    local playerId = keys.PlayerID

    local playerInfo = g_PlayerManager:getPlayerInfoForPlayerId(playerId)
    if playerInfo then
        PlayerManager:onPlayerReconnect(playerId)
    else
        self:logw("Player ID=" .. tostring(playerId) .. " reconnected but there is no playerInfo!")
    end
end

-- Dota Game Event Listener: "npc_spawned"
-- An NPC has spawned somewhere in game.  This includes heroes
-- Used to grab heroes once they spawn in
function GameMode:OnNPCSpawned(keys)
    local npc = EntIndexToHScript(keys.entindex)
    local unit_owner = npc:GetOwner()

    -- OnHeroInGame
    if npc:IsRealHero() and npc.bFirstSpawned == nil then
        npc.bFirstSpawned = true
        GameMode:OnHeroInGame(npc)
    end
end

-- Dota Game Event Listener: "dota_player_pick_hero"
-- A player picked a hero (this is happening before OnNPCSpawned)
-- Required for some PlayerResource setup
function GameMode:OnPlayerPickHero(keys)
    local hero_name = keys.hero
    local hero_entity = EntIndexToHScript(keys.heroindex)
    local player = EntIndexToHScript(keys.player)

    Timers:CreateTimer(0.5, function()
        local playerID = hero_entity:GetPlayerID() -- or player:GetPlayerID() if player is not disconnected
        if PlayerResource:IsFakeClient(playerID) then
            -- This is happening only for bots when they spawn for the first time or if they use custom hero-create spells (Custom Illusion spells)
        else
            if PlayerResource.PlayerData
               and PlayerResource.PlayerData[playerID]
               and PlayerResource.PlayerData[playerID].already_assigned_hero == true
            then
                -- This is happening only when players create new heroes with spells (Custom Illusion spells)
            else
                PlayerResource:AssignHero(playerID, hero_entity)
                PlayerResource.PlayerData[playerID].already_assigned_hero = true
            end
        end
    end)
end

-- Dota Game Event Listener: "entity_killed"
-- An entity died (an entity killed an entity)
-- Used to trigger onDeath events and disable kill cam
function GameMode:OnEntityKilled(keys)
    -- The Unit that was Killed
    local killedUnit = EntIndexToHScript(keys.entindex_killed)

    -- The Killing entity
    local killerEntity = nil

    if keys.entindex_attacker ~= nil then
        killerEntity = EntIndexToHScript( keys.entindex_attacker )
    end

    -- The ability/item used to kill, or nil if not killed by an item/ability
    local killerAbility = nil

    if keys.entindex_inflictor ~= nil then
        killerAbility = EntIndexToHScript(keys.entindex_inflictor)
        if killerAbility:GetClassname() ~= "ability_datadriven" then
            -- The ForceKill() command sometimes seems to set inflictor to a unit instead of an ability
            killerAbility = nil
        end
    end

    if killedUnit:GetTeamNumber() == DOTA_TEAM_GOODGUYS then
        -- Player hero died
        if killedUnit:IsHero() then
            -- Disable the Kill Cam
            local player = PlayerResource:GetPlayer(killedUnit:GetPlayerOwnerID())
            Timers:CreateTimer(1, function()
                -- Player may be null if player left the game
                if player and not player:IsNull() then
                    player:SetKillCamUnit(nil)
                end
            end)
        end
    end

    -- Units can add onDeathFunctions (see DotaUnitHelper) and these should be triggered now
    local onDeath = killedUnit.onDeath
    if onDeath then
        if type(onDeath) == "function" then
            onDeath(killedUnit, killerEntity, killerAbility)
        else
            -- Multiple functions to run
            for _,onDeathFunction in pairs(onDeath) do
                onDeathFunction(killedUnit, killerEntity, killerAbility)
            end
        end
    end
end
