
--[[
    Objective Markers Library 
    Version 0.0.1
    by Almouse

    Objective Markers are indicators on the ground designed to
    inform players of specific locations they should go to.

    Intended 0.1 functionality:
        -- Add markers to the ground using coordinates
        -- Register units that can "trigger" markers
        -- Run callback functions when a unit enters a marker region

    Later intended functionality:
        -- Show and configure markers to a specific subset of players
        -- Circular markers
        -- Improvements to particle appearance

]]


if ObjectiveMarkers == nil then
    print ( '[ObjectiveMarkers] creating ObjectiveMarkers' )
    ObjectiveMarkers = {}
end

function ObjectiveMarkers:initialize()
    self.initialized = true
end

-- TODO: Given two corners, ensure that they are botleft and topright
-- Useful for making assumptions elsewhere
function ObjectiveMarkers:SanitizeCorners(botleft, topright)

    return botleft, topright
end


-- Is a unit inside a rotated square?
-- TODO: Move to a utility library somewhere?
function ObjectiveMarkers:UnitIsWithinRotatedSquare(unit, botleft, topright, rotation)

    local unitOrigin = unit:GetAbsOrigin()

    -- Convert to 2d
    local x1 = botleft.x
    local x2 = topright.x
    local y1 = botleft.y
    local y2 = topright.y 


    local cos = math.cos(-rotation)
    local sin = math.sin(-rotation)

    -- Center of the square
    local mx = (x2-x1)/2
    local my = (y2-y1)/2

    -- Find the virtual coordinates of the corners when rot=0
    local x1u = cos*(x1-mx) - sin*(y1-my) + mx
    local y1u = sin*(x1-mx) + cos*(y1-my) + my
    local x2u = cos*(x2-mx) - sin*(y2-my) + mx
    local y2u = sin*(x2-mx) + cos*(y2-my) + my

    -- And the unit
    local unitx = cos*(unitOrigin.x-mx) - sin*(unitOrigin.y-my) + mx
    local unity = sin*(unitOrigin.x-mx) + cos*(unitOrigin.y-my) + my

    -- Check the converted unit coordinates are inside these coordinates
    return (unitx < x2u and
            unitx > x1u and
            unity > y1u and
            unity < y2u
            )

end

function ObjectiveMarkers:CreateObjectiveMarker(botleft, topright, color, rotation)

    -- Defaults
    local color = color or Vector(255, 0, 0)
    local rotation = rotation or 0

    --[[
        4 -cp1- 2
        |       |
        |       |
        |       |
        |       |
        1 -cp0- 3
    --]]

    -- Convert to 2d
    local x1 = botleft.x
    local x2 = topright.x
    local y1 = botleft.y
    local y2 = topright.y 

    local cos = math.cos(-rotation)
    local sin = math.sin(-rotation)

    -- Center of the square
    local mx = (x2-x1)/2
    local my = (y2-y1)/2

    -- Find the virtual coordinates of the corners when rot=0
    local x1u = cos*(x1-mx) - sin*(y1-my) + mx
    local y1u = sin*(x1-mx) + cos*(y1-my) + my
    local x2u = cos*(x2-mx) - sin*(y2-my) + mx
    local y2u = sin*(x2-mx) + cos*(y2-my) + my
    
    -- Find the width and height in this coordinate system
    local width = math.abs(x2u-x1u)
    local height = math.abs(y2u-y1u)

    -- Calculate the other two corners in the rotated coordinate system
    local x3u = x2u
    local y3u = y1u
    local x4u = x1u
    local y4u = y2u

    -- Reverse rotation direction
    cos = math.cos(rotation)
    sin = math.sin(rotation)

    -- Calculate the other two corners in the original coordinate system
    local botright = Vector(
        cos*(x3u-mx) - sin*(y3u-my) + mx
        ,sin*(x3u-mx) + cos*(y3u-my) + my
        ,0)

    local topleft = Vector(
        cos*(x4u-mx) - sin*(y4u-my) + mx
        ,sin*(x4u-mx) + cos*(y4u-my)  + my
        ,0)

    local scaledWidth = width*0.5

    local dummyOrigin = GetGroundPosition((botleft+botright)/2, nil)
    local shiftedOrigin = dummyOrigin+Vector(0,0,5) 
    local topOrigin = Vector((topleft.x+topright.x)/2, (topleft.y+topright.y)/2, shiftedOrigin.z)


    local particle = ParticleManager:CreateParticle( 
        "particles/scifi_mission/objective_marker_square.vpcf",
        PATTACH_WORLDORIGIN,
        nil
    )

    --[[
    0: Offset/origin
    1: No function
    2: x: height, y: width
    3: colour RGB
    4-7: corner locations (topleft anticlockwise)
    8-11: Corner rotations 

    ]]
    --print("Origins are: "..tostring(shiftedOrigin).. " and "..tostring(topOrigin))

    ParticleManager:SetParticleControl(particle, 0, shiftedOrigin)
    ParticleManager:SetParticleControl(particle, 1, topOrigin)
    ParticleManager:SetParticleControl(particle, 2, Vector(scaledWidth,0,0))
    ParticleManager:SetParticleControl(particle, 3, color)
    ParticleManager:SetParticleControl(particle, 4, Vector(topleft.x, topleft.y, shiftedOrigin.z+15))
    ParticleManager:SetParticleControl(particle, 5, Vector(botleft.x, botleft.y, shiftedOrigin.z+15))
    ParticleManager:SetParticleControl(particle, 6, Vector(botright.x, botright.y, shiftedOrigin.z+15))
    ParticleManager:SetParticleControl(particle, 7, Vector(topright.x, topright.y, shiftedOrigin.z+15))
    ParticleManager:SetParticleControl(particle, 8, Vector(rotation,0,0))
    ParticleManager:SetParticleControl(particle, 9, Vector(1.57+rotation,0,0))
    ParticleManager:SetParticleControl(particle, 10, Vector(3.14+rotation,0,0))
    ParticleManager:SetParticleControl(particle, 11, Vector(4.71+rotation,0,0))

    return particle

end

function ObjectiveMarkers:SetObjectiveMarkerColor(particle, color)

    ParticleManager:SetParticleControl(particle, 3, color)

end

function ObjectiveMarkers:ImmobiliseObjectiveMarker(particle, rotation, shouldImmobilise)
    local immobilise = shouldImmobilise and 100 or 0

    ParticleManager:SetParticleControl(particle, 8, Vector(rotation,0,immobilise))
    ParticleManager:SetParticleControl(particle, 9, Vector(1.57+rotation,0,immobilise))
    ParticleManager:SetParticleControl(particle, 10, Vector(3.14+rotation,0,immobilise))
    ParticleManager:SetParticleControl(particle, 11, Vector(4.71+rotation,0,immobilise))

end


if not ObjectiveMarkers.initialized then
    ObjectiveMarkers:initialize()
end
