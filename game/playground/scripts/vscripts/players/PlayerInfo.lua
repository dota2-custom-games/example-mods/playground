-- Basic data class containing information related to a specific player and their hero
-- This stores a ton of information about the player and their hero is used by a wide variety of systems (generally Player systems)

if not PlayerInfo then
    PlayerInfo = {}
end

function PlayerInfo:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    o.player = nil -- Player Entity
    o.playerId = -1 -- Player ID as assigned by Dota2
    o.playerIndex = -1  -- Player index. First player slot = 1, second player slot = 2.
    o.hero = nil -- The player's hero
    o.playerName = "<Unknown>"
    o.hasLoadedHero = false -- Set to true once BuildMarine has constructed a hero
    o.isConnected = true -- Set to false if the player is disconnected
    o.isKicked = false -- Set to true if the player was kicked
    o.timesReconnected = 0 -- Count of times the player has reconnected

    return o
end

-- Returns a PlayerInfo object using the passed in hero
function PlayerInfo:createInfoFromHero(hero)
    local playerId = hero:GetPlayerOwnerID()
    local player = PlayerResource:GetPlayer(playerId)
    local playerIndex = TeamToPlayerIndex(hero:GetTeamNumber())

    -- Initialize the Player Info Object
    local playerInfo = PlayerInfo:new()
    playerInfo.player = player
    playerInfo.playerId = playerId
    playerInfo.playerIndex = playerIndex
    playerInfo.playerName = PlayerResource:GetPlayerName(playerInfo.playerId) or "Unknown"

    -- Assign this info object to the player and hero entity itself for easy lookup
    player.playerInfo = playerInfo
    hero.playerInfo = playerInfo
    playerInfo.hero = hero

    return playerInfo
end
