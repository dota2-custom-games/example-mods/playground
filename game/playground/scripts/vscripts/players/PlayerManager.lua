-- The main manager of player related functions

-- Systems and classes PlayerManager is responsible for
require('players/PlayerInfo')
require('players/CustomStats')
require('players/HeroStateManager')
require('players/SettingsManager')

if not PlayerManager then
    PlayerManager = {}
    PlayerManager.__index = PlayerManager
    Log:addLogFunctions(PlayerManager, "PlayerManager")

    PlayerManager.allPlayerInfos = {}
    PlayerManager.kickedPlayersInfo = {} -- Stores playerInfos of players who have been kicked

    -- These can't be initialized until the pre game has started
    PlayerManager.playerCount = 0
    PlayerManager.playerCountIncludingDisconnects = 0
end

-- Called when the first player loads in and pregame has started
function PlayerManager:onPreGameStarted()
    -- Player count includes everyone on all custom teams as well (at this point since we haven't moved them to same team)
    self.playerCount = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_GOODGUYS)
    for i = DOTA_TEAM_CUSTOM_1, DOTA_TEAM_CUSTOM_8 do
        self.playerCount = self.playerCount + PlayerResource:GetPlayerCountForTeam(i)
    end

    -- Player count including all permanently disconnected players (kicked ones)
    self.playerCountIncludingDisconnects = self.playerCount

    self:logv("onPreGame | Player Count = " .. self.playerCount)
end

function PlayerManager:onPlayerHeroInGame(hero)
    local playerInfo = PlayerInfo:createInfoFromHero(hero)
    local player = playerInfo.player

    -- Add the player info so other systems can use it
    table.insert(self.allPlayerInfos, playerInfo)

    -- Update model
    HideWearables(hero)
    --RemoveAllSkills(hero)
    RemoveAllItems(hero) -- Damn dota, adding stupid items at the start
    InitializeInventory(hero)

    -- Move to GOOD_GUYS so they don't shoot each other
    player:SetTeam(DOTA_TEAM_GOODGUYS)
    hero:SetTeam(DOTA_TEAM_GOODGUYS)

    -- Set the initial gold to 0
    PlayerResource:SetGold(playerInfo.playerId, 0, true)
    PlayerResource:SetGold(playerInfo.playerId, 0, false)

    -- Add in custom stat system
    CustomStats:SetUpHeroForCustomStats(hero)
    hero:SetAbilityPoints(0)

    -- Add in hero state commands
    HeroStateManager:initializeUnit(hero)

    -- TODO:
    -- if g_SettingsManager:doesPlayerHaveSettings(playerInfo) then
    --     -- Have the settings manager rebroadcast the settings for the player (may update camera)
    --     g_SettingsManager:updateAllSettings(playerInfo)
    --     g_SettingsManager:sendPanoramaUpdate(playerInfo)
    -- end
end

-- Called once al players have loaded and have a hero in the game
function PlayerManager:onAllPlayersLoadedGame()
    self:logv("onAllPlayersLoadedGame")

    -- TODO: Still necessary?
    local keys = {}
    for _,playerInfo in pairs(self.allPlayerInfos) do
        keys[playerInfo.playerIndex] = playerInfo.playerId
    end
    CustomGameEventManager:Send_ServerToAllClients("all_players_loaded", keys )
end

function PlayerManager:onPlayerDisconnect(playerInfo)

    -- Check and see if this player was kicked
    if self:isPlayerKicked(playerInfo.playerId) then
        self:logv("onPlayerDisconnect: Player " .. tostring(playerInfo.playerId) .. " was kicked. Ignoring.")
        return
    end

    self:logi("onPlayerDisconnect: " .. tostring(playerInfo.playerId))

    local player = PlayerResource:GetPlayer(playerInfo.playerId)

    self.playerCount = self.playerCount - 1

    playerInfo.isConnected = false

    GameManager:onPlayerDisconnect(playerInfo)

    -- If all players are disconnected, pause the game (they might reconnect)
    local atLeastOnePlayerConnected = false
    for _,playerInfo in pairs(self.allPlayerInfos) do
        if playerInfo.isConnected then
            atLeastOnePlayerConnected = true
            break
        end
    end
    if not GameRules:IsGamePaused() and not atLeastOnePlayerConnected then
        print("PlayerManager | No players connected! Pausing game")
        PauseGame(true)
    end
end

-- Called when a player returns to the game after disconnecting
function PlayerManager:onPlayerReconnect(playerInfo)
    -- Check and see if this player was kicked
    if self:isPlayerKicked(playerId) then
        self:logv("onPlayerReconnect: Player " .. tostring(playerInfo.playerId) .. " reconnected but was previously was kicked. Ignoring.")
        return
    end

    self:logi("onPlayerReconnect: " .. tostring(playerInfo.playerId))

    -- Reassign the playerInfo field (since it gets lost on disconnect)
    local player = PlayerResource:GetPlayer(playerInfo.playerId)
    player.playerInfo = playerInfo
    playerInfo.player = player

    playerInfo.isConnected = true
    playerInfo.timesReconnected = playerInfo.timesReconnected + 1

    -- Dota likes giving gold to reconnecting players sometimes. So set it back to 0
    if not playerInfo.hasLoadedHero then
        PlayerResource:SetGold(playerInfo.playerId, 0, true)
        PlayerResource:SetGold(playerInfo.playerId, 0, false)
    end

    -- Put player back on the good guys in case they got moved
    -- Have to try a few times to ensure it works for some reason
    player:SetTeam(DOTA_TEAM_GOODGUYS)
    local attempts = 10
    Timers:CreateTimer(1, function()
        player:SetTeam(DOTA_TEAM_GOODGUYS)
        if attempts > 0 then
            attempts = attempts - 1
            return 1
        end
    end)

    local hero = player:GetAssignedHero()
    hero:SetTeam(DOTA_TEAM_GOODGUYS)
    hero:SetOwner(player)
    hero:SetControllableByPlayer(playerInfo.playerId, true)

    GameManager:onPlayerReconnect(playerInfo)

    -- Remove control from everyone else
    PlayerManager:setPlayerUnitsControllableByAll(playerInfo, false)
    Timers:CreateTimer(3, function()
        -- Remove control from everyone else, again, just in case the first one failed for some reason
        PlayerManager:setPlayerUnitsControllableByAll(playerInfo, false)
    end)
end

-- Called when a player is kicked
function PlayerManager:onPlayerKicked(playerInfo)
    table.insert(self.kickedPlayersInfo, playerInfo)
    for index, pi in pairs(self.allPlayerInfos) do
        if pi == playerInfo then
            table.remove(self.allPlayerInfos, index)
            break
        end
    end

    playerInfo.isKicked = true

    self.playerCount = self.playerCount - 1

    GameManager:onPlayerDisconnect(playerInfo)
end

-- Returns the player info for the supplied player id (or nil if there isn none)
function PlayerManager:getPlayerInfoForPlayerId(playerId)
    playerId = tonumber(playerId)
    for _,playerInfo in pairs(self.allPlayerInfos) do
        if tonumber(playerInfo.playerId) == playerId then
            return playerInfo
        end
    end
    for _,playerInfo in pairs(self.kickedPlayersInfo) do
        if tonumber(playerInfo.playerId) == playerId then
            return playerInfo
        end
    end
    return nil
end

-- Returns all players' heroes, including connected ones
function PlayerManager:getPlayerHeroes()
    local playerHeroes = {}
    for _,playerInfo in pairs(self.allPlayerInfos) do
        if playerInfo.hero then
            table.insert(playerHeroes, playerInfo.hero)
        end
    end
    return playerHeroes
end

-- Returns all connected players' heroes
function PlayerManager:getConnectedPlayerHeroes()
    local playerHeroes = {}
    for _,playerInfo in pairs(self.allPlayerInfos) do
        if playerInfo.isConnected and playerInfo.hero then
            table.insert(playerHeroes, playerInfo.hero)
        end
    end
    return playerHeroes
end

-- Returns true if the player id passed in was kicked
function PlayerManager:isPlayerKicked(playerId)
    for _,pi in pairs(self.kickedPlayersInfo) do
        if tonumber(pi.playerId) == playerId then
            return true
        end
    end
    return false
end

function PlayerManager:setPlayerUnitsControllableByAll(playerInfo, isControllable)
    local playerId = playerInfo.playerId
    self:logv("Setting control of units owned by " .. tostring(playerId) .. " to controllable by others == " .. tostring(isControllable))

    -- Share mask is: "1 shares heroes, 2 shares units, 4 disables help" to define why units will be controllable
    -- and the state boolean sets whether or not the units in the share mask should be controllable
    -- So Share on would be (3 (11),true) and share off is (3 (11), false)
    for _,otherPlayerInfo in pairs(self.allPlayerInfos) do
        if otherPlayerInfo.playerId ~= playerId then
            PlayerResource:SetUnitShareMaskForPlayer(playerId, otherPlayerInfo.playerId, 3, isControllable)
        end
    end
end
