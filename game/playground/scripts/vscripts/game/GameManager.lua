-- This file will be responsible as the "master" of the "game" logic and is the entry point
-- for setting up the game rounds themselves

-- Life Cycle Notes:
-- Map Managers should implement the following life cycle methods which will be called by GameManager:
-- onPreGameStarted()
--    Called when the game first starts. Map Managers should initialize some minor things and perhaps kick off votes
-- onAllPlayersLoadedGame()
--    Called once the last player has finished loading in.
-- onPlayerAdded(playerInfo)
--    Called when a player's hero is first created as well as when players reconnect
-- onPlayerRemoved(playerInfo)
--    Called when a player disconnects or is kicked

if not GameManager then
    GameManager = {}
    GameManager.__index = GameManager
    Log:addLogFunctions(GameManager, "GameManager")
end

-- Called when the first player loads in and preGame has started
function GameManager:onPreGameStarted()
    if SHOW_GAME_SYSTEM_LOGS then
        print("PreGame has started!")
    end

    local mapName = GetMapName()
    if string.find(mapName, "playground") then
        -- PlaygroundManager bypasses standard execution and the normal minigame system
        self:logv("Playground Map is loaded - Launching PlaygroundManager")
        self.minigameManager = PlaygroundManager:new()
    else
        error("ERROR | Could not find a map loader for map " .. tostring(mapName))
    end

end

-- Called when a player first finishes loading their hero
-- The hero should be initialized with playerInfo and setup code from PlayerManager
function GameManager:onPlayerHeroInGame(hero)
    -- self.mapMode:onPlayerAdded(hero.playerInfo)
    MinimapManager:registerHeroEntity(hero, "", true, "")
end

function GameManager:onAllPlayersLoadedGame()
    self:logv("onAllPlayersLoadedGame")
    self.minigameManager:onStart()
end

-- Called when a player disconnects
function GameManager:onPlayerDisconnect(playerInfo)
    self.minigameManager:onPlayerRemoved()
end

-- Called when a player reconnects
function GameManager:onPlayerReconnect(playerInfo)
    self.minigameManager:onPlayerAdded()
end
