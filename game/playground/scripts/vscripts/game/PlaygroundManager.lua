if not PlaygroundManager then
    PlaygroundManager = {}
end


function PlaygroundManager:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    self.playerID = nil
    self.heroUnit = nil

    return o

end

--[[ 
    These functions are required by GameManager.
    They are probably irrelevant for actual testing purposes
--]]
function PlaygroundManager:onPlayerHeroInGame()
end

function PlaygroundManager:onStart()
    -- Run the tests
    self:StartTests()
end

function PlaygroundManager:onPlayerDisconnect()

end

function PlaygroundManager:onPlayerReconnect()

end

--[[
    Main "Work" function, runs a bunch of interesting tests
--]]
function PlaygroundManager:StartTests()
    print("PlaygroundManager | Initialize!")
end