-- This is the entry-point to the game mode and used primarily to precache models/particles/sounds/etc
-- See: gamemode.lua for more game initialization logic

require('gamemode')

function Precache(context)
--[[
  This function is used to precache resources/units/items/abilities that will be needed
  for sure in your game and that will not be precached by hero selection.  When a hero
  is selected from the hero selection screen, the game will precache that hero's assets,
  any equipped cosmetics, and perform the data-driven precaching defined in that hero's
  precache{} block, as well as the precache{} block for any equipped abilities.

  See GameMode:PostLoadPrecache() in gamemode.lua for more information
  ]]

end

-- Create the game mode when we activate
function Activate()
	print("--- Game Started ---")
	GameMode:InitGameMode()
end
