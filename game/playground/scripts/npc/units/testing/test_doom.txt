"DOTAUnits"
{
//=================================================================================
    // Test Builder Unit
    //=================================================================================
    "test_doom"
    {
        // General
        "BaseClass"                 "npc_dota_creature" 
        "Model"                     "models/heroes/doom/doom.vmdl"
        //"SoundSet"                    "Undying_Zombie"
        "GameSoundsFile"            "soundevents/game_sounds_heroes/game_sounds_crystalmaiden.vsndevts"
        "ModelScale"                "1.5"
        "Level"                     "1"

        // Abilities
        //----------------------------------------------------------------
        "Ability1"                  "common_no_health_bar"
        "Ability2"                  ""
        "Ability3"                  ""
        "Ability4"                  ""
        "Ability5"                  ""
        "Ability6"                  ""          // Ability 6 - Extra.
        "Ability8"                  ""          // Ability 8 - Extra.

        // Armor
        //----------------------------------------------------------------
        "ArmorPhysical"             "0"         // Physical protection.
        "ArmorType"                 "unarmored"
        "MagicalResistance"         "0"     // Magical protection.

        // Attack
        //----------------------------------------------------------------
        "AttackCapabilities"        "DOTA_UNIT_CAP_NO_ATTACK"
        "AttackDamageMin"           "80"        // Damage range min.
        "AttackDamageMax"           "100"       // Damage range max.
        "AttackRate"                "1.20"      // Speed of attack.
        "AttackAnimationPoint"      "0.7"       // Normalized time in animation cycle to attack.
        "AttackAcquisitionRange"    "1800"      // Range within a target can be acquired.
        "AttackRange"               "60"        // Range within a target can be attacked.
        "AttackType"                "melee1"
        "ProjectileModel"           ""          // Particle system model for projectile.
        "ProjectileSpeed"           ""          // Speed of projectile.

        // Bounty
        //----------------------------------------------------------------
        "BountyXP"                  "0"     // Experience earn.
        "BountyGoldMin"             "0"     // Gold earned min.
        "BountyGoldMax"             "0"     // Gold earned max.

        // Bounds
        //----------------------------------------------------------------
        "RingRadius"                "32"
        "HealthBarOffset"           "120"
        "BoundsHullName"            "DOTA_HULL_SIZE_HUGE"

        // Movement
        //----------------------------------------------------------------
        "MovementCapabilities"      "DOTA_UNIT_CAP_MOVE_GROUND"         // Type of locomotion - ground, air
        "MovementSpeed"             "268"       // Speed
        "MovementTurnRate"          "0.5"       // Turning rate.

        // Status
        //----------------------------------------------------------------
        "StatusHealth"              "5000"       // Base health.
        "StatusHealthRegen"         "2000"      // Health regeneration rate.
        "StatusMana"                "250"           // Base mana.
        "StatusManaRegen"           ".5"            // Mana regeneration rate.

        // Team
        //----------------------------------------------------------------
        "TeamName"                  "DOTA_TEAM_NEUTRALS"            // Team name.
        "CombatClassAttack"         "DOTA_COMBAT_CLASS_ATTACK_HERO"
        "CombatClassDefend"         "DOTA_COMBAT_CLASS_DEFEND_HERO"
        "UnitRelationshipClass"     "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"

        // Vision
        //----------------------------------------------------------------
        "VisionDaytimeRange"        "600"       // Range of vision during day light.
        "VisionNighttimeRange"      "500"       // Range of vision at night time.

        // Creature Data
        //----------------------------------------------------------------------------------------------------------------------
        "Creature"
        {
            //Level Up Parameters
            //-----------------------------------------------------------------
            "HPGain"                    "0"
            "DamageGain"                "0"
            "ArmorGain"                 "0"
            "MagicResistGain"           "0"
            "MoveSpeedGain"             "0"
            "BountyGain"                "0"
            "XPGain"                    "0"

            "AttachWearables"
            {
                "Wearable1"     {   "ItemDef"       "232"      }
                "Wearable2"     {   "ItemDef"       "4523"       } 
            }
        }
    }
}
